import 'package:flutter/material.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({key}) : super(key: key);

  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
          Stack(
              children: <Widget>[
                Container(
                    height: MediaQuery.of(context).size.height - 20.0,
                    width: MediaQuery.of(context).size.width,
                    color: Color(0xFFF3B2B7)),


                Positioned(
                    top: MediaQuery.of(context).size.height /10,
                    left: 75.0,
                    child: Container(
                        height: 400.0,
                        width: 700.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/pinkcup.png'),
                                fit: BoxFit.cover
                            )
                        )
                    )
                ),
                Positioned(
                    top: 25.0,
                    left: 15.0,
                    child: Container(
                        height: 300.0,
                        width: 250.0,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //use a row with crossaxis as end
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    width: 150.0,
                                    child: Text('Caramel Macchiato',
                                        style: TextStyle(
                                            fontFamily: 'varela',
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white
                                        )
                                    ),
                                  ),
                                  SizedBox(width: 15.0),
                                  Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20.0),
                                          color: Colors.white
                                      ),
                                      child: Center(
                                          child: Icon(Icons.favorite, size: 18.0, color: Colors.red)
                                      )
                                  )
                                ],
                              ),
                              SizedBox(height: 10.0),
                              Container(
                                width: 170.0,
                                child: Text('Freshly steamed milk with vanilla-flavored syrup is marked with espresso and topped with caramel drizzle for an oh-so-sweet finish.',
                                    style: TextStyle(
                                        fontFamily: 'nunito',
                                        fontSize: 13.0,
                                        color: Colors.white
                                    )
                                ),
                              ),
                            ]
                        )
                    )
                ),
                Positioned(
                    top: MediaQuery.of(context).size.height / 3.3,
                    child: Container(
                      height: MediaQuery.of(context).size.height ,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(40.0),
                              topLeft: Radius.circular(40.0)),
                          color: Colors.white),)),

                Positioned(
                    top: MediaQuery.of(context).size.height / 3,
                    left: 15.0,
                    child: Container(
                        height: (MediaQuery.of(context).size.height),
                        width: MediaQuery.of(context).size.width,
                        child: ListView(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  Text('                                                                                                          '),

                                ],
                              ),
                              Container(child: Icon(Icons.close,size: 25,)
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 25.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      children: [
                                        Text(
                                          'Temperature                                       ',
                                          style: TextStyle(
                                              fontFamily: 'nunito',
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xFF726B68)),
                                        ),
                                      ],
                                    ),
                                    Container(child:
                                    Text(
                                      'Quantity       ',
                                      style: TextStyle(
                                          fontFamily: 'nunito',
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF726B68)),
                                    ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 7.0),
                          Column(children: [
                            Row(children: [

                            ],)

                          ],)

                        ],
                        )
                    )
                )
              ])
        ]
        )
    );
  }
}